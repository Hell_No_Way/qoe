package com.example.mobileq

const val DATABASE_VERSION: Int = 2
const val DATABASE_NAME: String = "CellInfo"
const val TABLE_NAME: String =  "milestones"

// Chores table columns names
const val KEY_ID: String = "id"
const val KEY_MILESTONE_LOCATION: String = "milestone_location"
const val KEY_MILESTONE_TECH: String = "milestone_technology"
const val KEY_MILESTONE_SIGNAL_STRENGTH: String = "milestone_signal_strength"
const val KEY_MILESTONE_C1: String = "milestone_c1"
const val KEY_MILESTONE_C2: String = "milestone_c2"
const val KEY_MILESTONE_RSCP: String = "milestone_rscp"
const val KEY_MILESTONE_ECNO: String = "milstone_ecno"
const val KEY_MILESTONE_RSRP: String = "milestone_rsrp"
const val KEY_MILESTONE_RSRQ: String = "milestone_rsrq"
const val KEY_MILESTONE_CINR: String = "milestone_rac"
const val KEY_MILESTONE_TAC: String = "milestone_tac"
const val KEY_MILESTONE_PLMN: String = "milestone_plmn"
const val KEY_MILESTONE_CELL_ID: String = "milestone_cell_id"
const val KEY_MILESTONE_COLOR: String = "milestone_color"
const val KEY_MILESTONE_DLR: String = "milestone_download"
const val KEY_MILESTONE_ULR: String = "milestone_upload"
const val KEY_MILESTONE_PING: String = "milestone_ping"
const val KEY_MILESTONE_JTTR: String = "milestone_jitter"

